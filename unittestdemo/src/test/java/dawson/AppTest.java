package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    //echo test
    @Test
    public void echoTest()
    {
        App a = new App();
        assertEquals( "Checking if the input's value is the one returned by method echo" , 5, a.echo(5) );
    }
    //oneMore test
    @Test
    public void oneMoreTest()
    {
        App b = new App();
        assertEquals("Testing if the returned value equals the input plus one or more", 7, b.oneMore(6));
    }
}
